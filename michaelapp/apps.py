from django.apps import AppConfig


class MichaelappConfig(AppConfig):
    name = 'michaelapp'
