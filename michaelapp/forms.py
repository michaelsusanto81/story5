from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):

	class Meta:
		model = Schedule
		fields = {'day', 'date', 'hour', 'subject', 'location', 'category'}
		labels = {
			'day' : 'Day ',
			'date' : 'Date ',
			'hour' : 'Time ',
			'subject' : 'Subject ',
			'location' : 'Location ',
			'category' : 'Category ',
		}
		widgets = {
			'day' : forms.TextInput(attrs={'placeholder' : 'ex: Monday'}),
			'date' : forms.DateInput(attrs={'placeholder' : 'format: YYYY-MM-DD'}),
			'hour' : forms.TimeInput(attrs={'placeholder' : 'format: HH:MM or HH:MM:SS'}),
			'subject' : forms.TextInput(attrs={'placeholder' : 'ex: PPW'}),
			'location' : forms.TextInput(attrs={'placeholder' : 'ex: Fasilkom 2304'}),
			'category' : forms.TextInput(attrs={'placeholder' : 'ex: Course'}),
		}
