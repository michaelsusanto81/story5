from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Schedule(models.Model):
	day = models.CharField(max_length=10)
	hour = models.TimeField()
	date = models.DateField()	
	subject = models.CharField(max_length=30)
	location = models.CharField(max_length=30)
	category = models.CharField(max_length=20)