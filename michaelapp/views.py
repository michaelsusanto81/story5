from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import Schedule

# Create your views here.
def index(request):
	return render(request, 'index.html')

def portfolio(request):
	response = {'':''}
	return render(request, 'portfolio.html')

def about(request):
	response = {'':''}
	return render(request, 'about.html')

def hireMe(request):
	response = {'':''}
	return render(request, 'hire-me.html')

def schedule(request):
	form = ScheduleForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response = {}
		response['day'] = request.POST['day']
		response['date'] = request.POST['date']
		response['hour'] = request.POST['hour']
		response['subject'] = request.POST['subject']
		response['location'] = request.POST['location']
		response['category'] = request.POST['category']
		schedule = Schedule(day=response['day'], date=response['date'], hour=response['hour'], subject=response['subject'], location=response['location'], category=response['category'])
		schedule.save()
		schedules = Schedule.objects.all()
		response = {'schedules' : schedules}
		return render(request, 'schedule.html', response)
	else:
		schedules = Schedule.objects.all()
		response = {'schedules' : schedules}
		return render(request, 'schedule.html', response)

def addSchedule(request):
	response = {'form' : ScheduleForm}
	return render(request, 'addSchedule.html', response)

def removeSchedule(request, schedule_id):
	schedule = Schedule.objects.get(id=schedule_id)
	schedule.delete()
	return redirect('/schedule/')